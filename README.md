


# Alps iOS API


## Requirements


## Installation

Alps iOS API pod is available through [CocoaPods](http://cocoapods.org). To install it,
simply add the following line to your Podfile:

    pod "Alps", :git => 'https://bitbucket.org/alps/ios-api.git, :tag => 'v0.0.2'


## Authors

rk, rafal.kowalski@mac.com


## License

Alps iOS API pod is available under the MIT license. See the LICENSE file for more info.
